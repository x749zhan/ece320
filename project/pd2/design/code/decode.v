module decode (
    input wire clock,
    input wire [31:0] f_pc,
    input wire [31:0] inst,
    output reg [31:0] d_pc,
    output reg [6:0] opcode,
    output reg [4:0] rd,
    output reg [4:0] rs1,
    output reg [4:0] rs2,
    output reg [2:0] funct3,
    output reg [6:0] funct7,
    output reg [31:0] imm,
    output reg [4:0] shamt
);

initial begin
    opcode = inst[6:0];
    d_pc = f_pc;
    opcode = 0;
    rd = 0;
    rs1 = 0;
    rs2 = 0;
    funct3 = 0;
    funct7 = 0;
    shamt = 0;
    imm = 0;
end

always @(inst) begin
    opcode = inst[6:0];
    d_pc = f_pc;
    case(opcode)
    // R type done
        7'b0110011 : begin
            rd = inst[11:7];
            funct3= inst[14:12];
            rs1 = inst[19:15];
            rs2 = inst[24:20];
            funct7 = inst[31:25];
            imm = 0;
            shamt = 0;
        end
    // I type JALR
        7'b1100111 : begin
            rd = inst[11:7];
            funct3= inst[14:12];
            rs1 = inst[19:15];
            rs2 = 0;
            funct7 = 0;
            shamt = 0;
            imm[11:0] = inst[31:20];
            // sign extension
            if(inst[31]==1'b1) begin
                imm[31:12] = 20'hFFFFF;
            end
            else imm[31:12] = 0;
        end
    // I type
        7'b0000011 : begin
            rd = inst[11:7];
            funct3= inst[14:12];
            rs1 = inst[19:15];
            rs2 = 0;
            funct7 = 0;
            shamt = 0;
            imm[11:0] = inst[31:20];
            // sign extension
            if(inst[31]==1'b1) begin
                imm[31:12] = 20'hFFFFF;
            end
            else imm[31:12] = 0;
        end
    // I type
        7'b0010011 : begin
            rd = inst[11:7];
            funct3= inst[14:12];
            rs1 = inst[19:15];
            rs2 = 0;
            funct7 = 0;
            // all I type including ADDI
            imm[11:0] = inst[31:20];
            // sign extension
            if(inst[31]==1'b1) begin
                imm[31:12] = 20'hFFFFF;
            end
            else imm[31:12] = 0;
            shamt = 0;
            // SLLI
            if(funct3 == 3'b001) begin
                shamt = inst[24:20];
            end
            else if(funct3 == 3'b101) begin
                // SRAI
                if(inst[30]==1'b1) begin
                    shamt = inst[24:20];
                end
                // SRLI
                else if(inst[30]==1'b0) begin
                    shamt = inst[24:20];
                end
            end
        end
    // S type
        7'b0100011 : begin
            imm[4:0]= inst[11:7];
            funct3= inst[14:12];
            rs1 = inst[19:15];
            rs2 = inst[24:20];
            rd = 0;
            funct7 =0;
            shamt = 0;
            imm[11:5] = inst[31:25];
            // sign extension
            if(inst[31]==1'b1) begin
                imm[31:12] = 20'hFFFFF;
            end
            else imm[31:12] = 0;
        end
    // B type
        7'b1100011 : begin
            rd = 0;
            funct7 =0;
            shamt = 0;
            imm[0] = 0;
            imm[4:1] = inst[11:8];
            imm[11] = inst[7];
            funct3= inst[14:12];
            rs1 = inst[19:15];
            rs2 = inst[24:20];
            imm[12] = inst[31];
            imm[10:5] = inst[30:25];
            // sign extension
            if(inst[31]==1'b1) begin
                imm[31:12] = 20'hFFFFF;
            end
            else imm[31:12] = 0;
        end
    // U type AUPIC
        7'b0010111 : begin
            rs1 = 0;
            rs2 = 0;
            funct3 = 0;
            funct7 =0;
            shamt = 0;
            rd = inst[11:7];
            imm[31:12] = inst[31:12];
            // zero extension
            imm[11:0] = 0;
        end
    // U type LUI
        7'b0110111 : begin
            rs1 = 0;
            rs2 = 0;
            funct3 = 0;
            funct7 =0;
            shamt = 0;
            rd = inst[11:7];
            imm[31:12] = inst[31:12];
            // zero extension
            imm[11:0] = 0;
        end
    // J type JAL
        7'b1101111 : begin
            rs1 = 0;
            rs2 = 0;
            funct3 = 0;
            funct7 =0;
            shamt = 0;
            rd = inst[11:7];
            imm[19:12] = inst[19:12];
            imm[11] = inst[20];
            imm[10:1] = inst[30:21];
            imm[20] = inst[31];
        end
        default begin
            rd = 0;
            funct3= 0;
            rs1 = 0;
            rs2 = 0;
            funct7 = 0;
            imm = 0;
            shamt = 0;
        end
    endcase
end
endmodule