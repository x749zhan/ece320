module branch_comp (
    input wire [31:0] rs1,
    input wire [31:0] rs2,
    input wire BrUn,
    output reg BrEq,
    output reg BrLT
);
    always @(rs1 or rs2 or BrUn) begin
        // equal
        if(rs1 == rs2) begin
            BrEq = 1'b1;
            BrLT = 0;
        end
        // unsigned comparison
        else if (BrUn == 1'b1) begin
            if(rs1 < rs2) begin 
                BrLT = 1'b1;
                BrEq = 0;
            end
            else begin
                BrLT = 0;
                BrEq = 0;
            end
        end
        // signed comparison
        else if (BrUn == 0) begin
            if($signed(rs1) < $signed(rs2)) begin
                BrLT = 1'b1;
                BrEq = 0;
            end
            else begin
                BrLT = 0;
                BrEq = 0;
            end
        end
        else begin
            BrLT = 0;
            BrEq = 0;
        end
    end
endmodule