module pd(
  input clock,
  input reset
);

/** 
  fetch
**/
reg [31:0] pc = 32'h01000000;
reg [31:0] e_pc = pc;
reg [31:0] d_pc;
reg [31:0] f_insn;
wire [31:0] data_in;
wire read_write;

always @(posedge clock) begin
  if(reset) begin
    pc <= 32'h01000000;
  end
  else if (PCSel == 1'b1) begin
    pc <= e_alu_res;
  end
  else begin
    pc <= pc + 4;
  end
end

imemory imemory_0(
  .clock(clock),
  .address(pc),
  .data_in(data_in),
  .data_out(f_insn),
  .read_write(read_write)
);
/**
  fetch end
**/

/** 
  decode + imm gen
**/
reg [6:0] d_opcode;
reg [4:0] d_rd;
reg [4:0] d_rs1;
reg [4:0] d_rs2;
reg [2:0] d_funct3;
reg [6:0] d_funct7;
reg [31:0] d_imm;
reg [4:0] d_shamt;

decode decode_0(
  .clock(clock),
  .f_pc(pc),
  .inst(f_insn),
  .d_pc(d_pc),
  .opcode(d_opcode),
  .rd(d_rd),
  .rs1(d_rs1),
  .rs2(d_rs2),
  .funct3(d_funct3),
  .funct7(d_funct7),
  .imm(d_imm),
  .shamt(d_shamt)
);
/** 
  decode + imm gen end
**/

/**
  control
**/
reg PCSel;
reg [31:0] e_alu_res;

reg r_write_enable;
reg [4:0] r_write_destination = d_rd;
reg [31:0] r_write_data;
reg [4:0] r_read_rs1 = d_rs1;
reg [4:0] r_read_rs2 = d_rs2;
reg [31:0] r_read_rs1_data;
reg [31:0] r_read_rs2_data;

reg BrUn;
reg BrEq;
reg BrLT;
reg r_br_taken;

reg [1:0] ASel;
reg [1:0] BSel;
reg [31:0] x;
reg [31:0] y;
reg [3:0] ALUSel;

control_signal control_signal_0(
    .inst(f_insn),
    .BrEq(BrEq),
    .BrLT(BrLT),
    .PCSel(PCSel),
    .BSel(BSel),
    .ASel(ASel),
    .ALUSel(ALUSel),
    .write_enable(r_write_enable),
    .branch_taken(r_br_taken)
);
/**
  control end
**/

/** 
  reg file
**/

register_file register_file_0(
    .clock(clock),
    .addr_rs1(r_read_rs1),
    .addr_rs2(r_read_rs2),
    .addr_rd(r_write_destination),
    .data_rd(r_write_data),
    .data_rs1(r_read_rs1_data),
    .data_rs2(r_read_rs2_data),
    .write_enable(r_write_enable)
);
/** 
  reg file end
**/

/**
  set BrUn
**/
always @(f_insn) begin
  // B type
  if(f_insn[6:0] == 7'b1100011) begin
    // BLTU
    if(f_insn[14:12] == 3'b110) begin
      BrUn = 1'b1;
    end
    // BGEU
    else if(f_insn[14:12] == 3'b111) begin
      BrUn = 1'b1;
    end
    else begin
      BrUn = 0;       
    end
  end
  else begin
    BrUn = 0;
  end
end
/**
  set BrUn end
**/

/** 
  branch
**/
branch_comp branch_comp_0(
  .rs1(r_read_rs1_data),
  .rs2(r_read_rs2_data),
  .BrUn(BrUn),
  .BrEq(BrEq),
  .BrLT(BrLT)
);
/** 
  branch end
**/

/** 
  alu
**/
alu alu_0(
  .x(x),
  .y(y),
  .ALUSel(ALUSel),
  .z(e_alu_res)
);
/** 
  alu end
**/

/** 
  alu input mux
**/
alu_input_mux alu_input_mux_0(
  .BSel(BSel),
  .ASel(ASel),
  .pc(pc),
  .rs1(r_read_rs1_data),
  .rs2(r_read_rs2_data),
  .imm(d_imm),
  .shamt(d_shamt),
  .x(x),
  .y(y)
);
/** 
  alu input mux end
**/

endmodule
