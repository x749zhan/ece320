/**
ASel
11 pc
10 rs1
00 0
BSel
11 imm
10 rs2
00 shamt
**/
module alu_input_mux (
    input wire [1:0] BSel,
    input wire [1:0] ASel,
    input wire [31:0] pc,
    input wire [31:0] rs1,
    input wire [31:0] rs2,
    input wire [31:0] imm,
    input wire [4:0] shamt,
    output reg [31:0] x,
    output reg [31:0] y
);
always @(ASel or BSel) begin
    if (ASel == 2'b11 && BSel == 2'b11) begin
        x = pc;
        y = imm;
    end
    else if (ASel == 2'b11 && BSel == 2'b10) begin
        x = pc;
        y = rs2;
    end
    else if (ASel == 2'b11 && BSel == 0) begin
        x = pc;
        y[4:0] = shamt;
        y[31:5] = 0;
    end

    else if (ASel == 2'b10 && BSel == 2'b11) begin
        x = rs1;
        y = imm;
    end
    else if (ASel == 2'b10 && BSel == 2'b10) begin
        x = rs1;
        y = rs2;
    end
    else if (ASel == 2'b10 && BSel == 0) begin
        x = rs1;
        y[4:0] = shamt;
        y[31:5] = 0;
    end

    else if (ASel == 0 && BSel == 2'b11) begin
        x = 0;
        y = imm;
    end
    else if (ASel == 0 && BSel == 2'b10) begin
        x = 0;
        y = rs2;
    end
    else if (ASel == 0 && BSel == 0) begin
        x = 0;
        y[4:0] = shamt;
        y[31:5] = 0;
    end

    else begin
        x = pc;
        y = imm;
    end
end
    
endmodule