module register_file (
    input wire clock,
    input wire [4:0] addr_rs1,
    input wire [4:0] addr_rs2,
    input wire [4:0] addr_rd,
    input wire [31:0] data_rd,
    output reg [31:0] data_rs1,
    output reg [31:0] data_rs2,
    input wire write_enable
);

reg [31:0] reg_file [0:31];
initial begin
    data_rs1 = 0;
    data_rs2 = 0;
end

    // read
always @(addr_rs1 or addr_rs2) begin
    data_rs1 = reg_file[addr_rs1];
    data_rs2 = reg_file[addr_rs2];
end

    // write
always @(posedge clock) begin
    if(write_enable == 1'b1) begin
        reg_file[addr_rd] <= data_rd;
    end
end
endmodule