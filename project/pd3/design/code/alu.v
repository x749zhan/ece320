/**
0000 add
0001 sub
0010 and
0011 or
0100 xor
0101 left shift
0110 right shift
0111 compare signed
1000 compare unsigned
1001 left shit arith
1010 right shift arith
**/
module alu (
    input wire [31:0] x, // rs1 or pc
    input wire [31:0] y, // rs2 or imm
    input wire [3:0] ALUSel,
    output reg [31:0] z
);
    always @(x or y) begin
        case(ALUSel)
        // add
            4'b0000 : begin
               z = $signed(x) + $signed(y);
            end
        // sub
            4'b0001 : begin
                z = $signed(x) - $signed(y);
            end
        // and
            4'b0010 : begin
                z = x & y ;
            end
        // or
            4'b0011 : begin
                z = x | y;
            end
        // xor
            4'b0100 : begin
                z = x ^ y;
            end
        // left shift
            4'b0101 : begin
                z = x << y;
            end
        // right shift
            4'b0110 : begin
                z = x >> y;
            end
        // compare signed
             4'b0111 : begin
                if($signed(x) < $signed(y)) begin
                    z = 32'h00000001;
                end
                else begin
                    z = 0;
                end
            end
        // compare unsigned
             4'b1000 : begin
                if(x < y) begin
                    z = 32'h00000001;
                end
                else begin
                    z = 0;
                end
            end
        // 1001 left shit arith
            4'b1001 : begin
                z = $signed(x) <<< y;
            end
        // 1010 right shift arith
            4'b1010 : begin
                z = $signed(x) >>> y;
            end
            default begin
                z = 0;
            end
        endcase
    end
endmodule