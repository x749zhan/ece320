/**
ALU signal
0000 add
0001 sub
0010 and
0011 or
0100 xor
0101 left shift
0110 right shift
0111 compare signed
1000 compare unsigned
1001 left shit arith
1010 right shift arith
**/
/**
ASel
11 pc
10 rs1
00 0
BSel
11 imm
10 rs2
00 shamt
**/
/**
PCSel
1 alu
0 pc+4
**/
module control_signal(

     // control signals
    input wire [31:0] inst,
    input wire BrEq,
    input wire BrLT,
    output reg PCSel,
    output reg [1:0] BSel,
    output reg [1:0] ASel,
    output reg [3:0] ALUSel,
    output reg write_enable,
    output reg branch_taken
);
reg [6:0] opcode;
initial begin
    opcode = 0;
    PCSel = 0;
    BSel = 0;
    ASel = 0;
    ALUSel = 0;
    write_enable = 0;
    branch_taken = 0;
end

always @(inst or BrEq or BrLT) begin
    opcode = inst[6:0];
    branch_taken = 0;
    case(opcode)
    // R type done
        7'b0110011 : begin
            // TODO: write_enable = 1'b1;
            write_enable = 0;
            PCSel = 0;
            ASel = 2'b10;
            BSel = 2'b10;
            // ADD
            if(inst[14:12] == 3'b000 && inst[30] == 0) begin
                ALUSel = 4'b0000;
            end
            // SUB
            else if(inst[14:12] == 3'b000 && inst[30] == 1'b1) begin
                ALUSel = 4'b0001;
            end
            // SLL
            else if(inst[14:12] == 3'b001) begin
                ALUSel = 4'b0101;
            end
            // SLT
            else if(inst[14:12] == 3'b010) begin
                ALUSel = 4'b0111;
            end
            // SLTU
            else if(inst[14:12] == 3'b011) begin
                ALUSel = 4'b1000;
            end
            // XOR
            else if(inst[14:12] == 3'b100) begin
                ALUSel = 4'b0100;
            end
            // SRL
            else if(inst[14:12] == 3'b101 && inst[30] == 0) begin
                ALUSel = 4'b0110;
            end
            // SRA
            else if(inst[14:12] == 3'b101 && inst[30] == 1'b1) begin
                ALUSel = 4'b1010;
            end
            // OR
            else if(inst[14:12] == 3'b110) begin
                ALUSel = 4'b0011;
            end
            // AND
            else if(inst[14:12] == 3'b111) begin
                ALUSel = 4'b0010;
            end
            else begin
                ALUSel = 4'b0000;
            end
        end
    // I type JALR done
        7'b1100111 : begin
            BSel = 2'b11;
            ASel = 2'b10;
            ALUSel = 4'b0000;
            // TODO: write_enable = 1'b1;
            write_enable = 0;
            PCSel = 1'b1;
        end
    // I type load done
        7'b0000011 : begin
            BSel = 2'b11;
            ASel = 2'b10;
            ALUSel = 4'b0000;
            // TODO: write_enable = 1'b1;
            write_enable = 0;
            PCSel = 0;
        end
    // I type done
        7'b0010011 : begin
            BSel = 2'b11;
            ASel = 2'b10;
            // TODO: write_enable = 1'b1;
            write_enable = 0;
            PCSel = 0;
            // ADDI ADD
            if(inst[14:12] == 3'b000) begin
                ALUSel = 4'b0000;
            end
            // SLTI COMPARE
            else if(inst[14:12] == 3'b010) begin
                ALUSel = 4'b0111;
            end
            // SLTIU COMPARE UNSIGNED
            else if(inst[14:12] == 3'b011) begin
                ALUSel = 4'b1000;
            end
            // XORI XOR
            else if(inst[14:12] == 3'b100) begin
                ALUSel = 4'b0100;
            end
            // ORI OR
            else if(inst[14:12] == 3'b110) begin
                ALUSel = 4'b0011;
            end
            // ANDI AND
            else if(inst[14:12] == 3'b111) begin
                ALUSel = 4'b0010;
            end
            // SLLI LEFT SHIT
            else if(inst[14:12] == 3'b001) begin
                BSel = 0;
                ALUSel = 4'b0101;
            end
            // SRLI RIGHT SHIFT
            else if(inst[14:12] == 3'b101 && inst[30] == 0) begin
                BSel = 0;
                ALUSel = 4'b0110;
            end
            // SRAI RIGHT SHIFT ARITH
            else if(inst[14:12] == 3'b101 && inst[30] == 1'b1) begin
                BSel = 0;
                ALUSel = 4'b1010;
            end
            else begin
                ALUSel = 4'b0000;
            end
        end
    // S type done
        7'b0100011 : begin
            BSel = 2'b11;
            ASel = 2'b10;
            ALUSel = 4'b0000;
            write_enable = 0;
            PCSel = 0;
        end
    // B type done
        7'b1100011 : begin
            BSel = 2'b11;
            ASel = 2'b11;
            write_enable = 0;
            ALUSel = 4'b0000;
            // BEQ
            if(inst[14:12] == 3'b000) begin
                if(BrEq == 1'b1) begin
                    PCSel = 1'b1;
                    branch_taken = 1'b1;
                end
                else begin
                    PCSel = 0;
                    branch_taken = 0;
                end
            end
            // BNE
            else if(inst[14:12] == 3'b001) begin
                if(BrEq == 0) begin
                    PCSel = 1'b1;
                    branch_taken = 1'b1;
                end
                else begin
                    PCSel = 0;
                    branch_taken = 0;
                end
            end
            // BLT
            else if(inst[14:12] == 3'b100) begin
                if(BrLT == 1'b1) begin
                    PCSel = 1'b1;
                    branch_taken = 1'b1;
                end
                else begin
                    PCSel = 0;
                    branch_taken = 0;
                end
            end
            // BGE
            else if(inst[14:12] == 3'b101) begin
                if(BrLT == 0) begin
                    PCSel = 1'b1;
                    branch_taken = 1'b1;
                end
                else begin
                    PCSel = 0;
                    branch_taken = 0;
                end
            end
            // BLTU
            else if(inst[14:12] == 3'b110) begin
                if(BrLT == 1'b1) begin
                    PCSel = 1'b1;
                    branch_taken = 1'b1;
                end
                else begin
                    PCSel = 0;
                    branch_taken = 0;
                end
            end
            // BGEU
            else if(inst[14:12] == 3'b111) begin
                if(BrLT == 0) begin
                    PCSel = 1'b1;
                    branch_taken = 1'b1;
                end
                else begin
                    PCSel = 0;
                    branch_taken = 0;
                end
            end
            else begin
                PCSel = 0;
                branch_taken = 0;
            end
        end
    // U type AUPIC done
        7'b0010111 : begin
            ASel = 2'b11;
            BSel = 2'b11;
            ALUSel = 4'b0000;
            // TODO: write_enable = 1'b1;
            write_enable = 0;
            PCSel = 0;
        end
    // U type LUI done
        7'b0110111 : begin
            ASel = 0;
            BSel = 2'b11;
            ALUSel = 4'b0000;
            // TODO: write_enable = 1'b1;
            write_enable = 0;
            PCSel = 0;
        end
    // J type JAL done
        7'b1101111 : begin
            ASel = 2'b11;
            BSel = 2'b11;
            ALUSel = 4'b0000;
            // TODO: write_enable = 1'b1; select PC+4 to rd
            write_enable = 0;
            PCSel = 1'b1;
        end
        default begin
            ASel = 0;
            BSel = 0;
            ALUSel = 0;
            write_enable = 0;
            PCSel = 0;
        end
    endcase
end
    
endmodule
