/**
* Exercise 3.4
* you can change the code below freely
  * */
module reg_and_reg(
  input wire clock,
  input wire reset,
  input wire x,
  input wire y,
  output reg z
);
reg x_0;
reg y_0;
always @(posedge clock)
begin
  if(reset)
  begin
    x_0 <= 0;
    y_0 <= 0;
    z <= 0;
  end
  else
  begin
    x_0 <= x;
    y_0 <= y;
    z <= x_0 & y_0;
  end
end

endmodule
