/**
* Exercise 3.3 
* you can change the code below freely
  * */
module reg_and_arst(
  input  wire clock,
  input  wire areset,
  input  wire x,
  input  wire y,
  output reg  z
);
  // always @(*) begin
  //   z = x & y;
  // end
  // removing posedge clock makes the reset function less "sensitive" to reset, why
always @(posedge clock or posedge areset) begin
  if(areset==1'b1) begin
    z <= 1'b0;
  end
  else
    z <= x & y;
end
endmodule
