module pd(
  input clock,
  input reset
);



reg [31:0] f_pc = 32'h01000000;
reg [31:0] f_insn;
wire [31:0] data_in;
wire read_write;

always @(posedge clock) begin
  if(reset) begin
    f_pc <= 32'h01000000;
  end
  else
    f_pc <= f_pc + 4;
end

imemory imemory_0(
  .clock(clock),
  .address(f_pc),
  .data_in(data_in),
  .data_out(f_insn),
  .read_write(read_write)
);
endmodule
