module imemory (
    input wire clock,
    input wire [31:0] address,
    input wire [31:0] data_in,
    output reg [31:0] data_out,
    input wire read_write

);
// little endian
reg [31:0] pc;
reg [31:0] tmp_array [0:`LINE_COUNT-1];
reg [7:0] memory [0:`MEM_DEPTH-1];
reg [31:0] offset = 32'h01000000;
integer i;
integer j;
integer k;

initial begin
// ins range [0x01000000,0x01100000] 
    $readmemh(`MEM_PATH, tmp_array);
    pc = address - offset;
// copy ins from tmp_array to memory
    for(i =0; i<`LINE_COUNT; i=i+1) begin
        k = i*4;
        for(j =0; j<8; j=j+1) begin
            memory[k][j] = tmp_array[i][j];
        end
        for(j =0; j<8; j=j+1) begin
            memory[k+1][j] = tmp_array[i][j+8];
        end
        for(j =0; j<8; j=j+1) begin
            memory[k+2][j] = tmp_array[i][j+16];
        end
        for(j =0; j<8; j=j+1) begin
            memory[k+3][j] = tmp_array[i][j+24];
        end
    end

    // for(i =0; i<10; i=i+1) begin
    //     $display("memory:");
    //     for(j =0; j<8; j=j+1) begin
    //         $display("%d",memory[i][j]);
    //     end
    // end
end

// read
always @(address)
begin
  pc = address-offset;
  if(pc > (`LINE_COUNT-1)*4) begin
    $finish;
  end
  else begin
    for(i = 0; i <8; i=i+1)
    begin
      data_out[i] = memory[pc][i];
    end
    for(i = 0; i <8; i=i+1)
    begin
      data_out[i+8] = memory[pc+1][i];
    end
    for(i = 0; i <8; i=i+1)
    begin
      data_out[i+16] = memory[pc+2][i];
    end
    for(i = 0; i <8; i=i+1)
    begin
      data_out[i+24] = memory[pc+3][i];
    end
  end
end

    
endmodule